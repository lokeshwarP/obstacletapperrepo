﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsatcleSpawner : MonoBehaviour
{
    public GameObject[] obstacle;
    public float timeBetweenSpawn;
    private float spawnTime;
    private int index;

    private void Start()
    {
        InvokeRepeating("Spwan", spawnTime, timeBetweenSpawn);
    }
    

    private void Spwan()
    {
        index = Random.Range(0,obstacle.Length);
        Instantiate(obstacle[index], transform.position, transform.rotation);
        
    }


}
