﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSelector : MonoBehaviour
{
     public Material[] colourGradient;
    
    int index;

    private void Start()
    {
        ColorSeleterOfQuad();
    }
    public void ColorSeleterOfQuad()
    {
        index = Random.Range(0, colourGradient.Length);
        GetComponent<Renderer>().material = colourGradient[index] ;
    }
}
