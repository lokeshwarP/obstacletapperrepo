﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    private Rigidbody2D rb;

    public float jumpStrength=10f;

    public AudioClip tapSFX;
    [SerializeField] [Range(0, 1)] float tapSFXVolume = 0.45f;
    public AudioClip dieSFX;
    [SerializeField] [Range(0, 1)] float dieSFXVolume = 0.75f;

    public Text scoreText;
    public float score = 0;

    public GameObject gameoverPanel;
    public Text scoreTextInPanel;

    public GameObject tapHereCanvas;


    // Start is called before the first frame update
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Start()
    {
        scoreText.text = score.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Destroy(tapHereCanvas);
            Jump();
        }
    }

    private void Jump()
    {
        AudioSource.PlayClipAtPoint(tapSFX, Camera.main.transform.position, tapSFXVolume);
        rb.velocity = Vector2.up * jumpStrength;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Obstacle")
        {
            Destroy(scoreText);
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(dieSFX, Camera.main.transform.position, dieSFXVolume);
            gameoverPanel.SetActive(true);
            scoreTextInPanel.text = score.ToString();
            if (PlayerPrefs.GetFloat("HighScore") < score)
            {
                PlayerPrefs.SetFloat("HighScore", score);
            }
        }
        
    }
    public void ScoreAdder()
    {
        score++;
        scoreText.text = score.ToString();
    }
}
