﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    
    public float cameraSpeed = 2f;
    


    // Update is called once per frame
    void Update()
    {
        if (FindObjectOfType<Player>().gameObject.activeInHierarchy)
        {
            transform.position += new Vector3(0, cameraSpeed * Time.deltaTime, 0);
        }
        else
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }

    }
   
}
  

