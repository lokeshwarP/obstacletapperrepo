﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingBackground : MonoBehaviour
{
    public float backgroundSpeed = 2f;
    public Renderer BackGroundRedender;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            BackGroundRedender.material.mainTextureOffset += new Vector2(0, backgroundSpeed * Time.deltaTime);
        }
    }

}
