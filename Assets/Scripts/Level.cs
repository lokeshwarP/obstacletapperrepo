﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
   public Text highScoreText;
     void Start()
    {
        highScoreText.text = PlayerPrefs.GetFloat("HighScore").ToString();
    }
    public  void LoadGame()
    {
        SceneManager.LoadScene("GameScene");
    }
    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
