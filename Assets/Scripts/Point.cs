﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Point : MonoBehaviour
{
    public AudioClip pointSFX;
    [SerializeField] [Range(0, 1)] float pointSFXVolume = 0.75f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(pointSFX, Camera.main.transform.position, pointSFXVolume);
            FindObjectOfType<Player>().ScoreAdder();
        }
    }
}
